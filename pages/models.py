from django.db import models
from django.utils.translation import gettext as _

from core.abstract_models import ContentAbstract


class Page(ContentAbstract):
    class Meta:
        verbose_name = _('Page')
        verbose_name_plural = _('Pages')

    def __str__(self):
        return self.title
