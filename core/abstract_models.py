from re import split

from django.db import models
from django.utils.translation import gettext as _

from .utils import slug_generator


class SingletonAbstract(models.Model):
    """ An abstract model for forcing models to be singleton. """
    def save(self, *args, **kwargs):
        self.pk = 1
        super().save(*args, **kwargs)

    @classmethod
    def get_object(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj

    class Meta:
        abstract = True


class FormAbstract(models.Model):
    """ An abstract model for different form models."""
    name = models.CharField(_("User's Name"), max_length=200)
    email = models.EmailField(_("User's Email"))
    date = models.DateTimeField(_("Send Time"), auto_now_add=True)

    class Meta:
        abstract = True
        ordering = ['-date']


class ContentAbstract(models.Model):
    """ An abstract model for content models like pages, blog posts etc.. """
    title = models.CharField(_("Title"), max_length=128)
    slug = models.SlugField(_("URL"), max_length=25, null=True, blank=True)
    thumbnail = models.ImageField(_("Thumbnail"), upload_to="thumbnails/%Y/%m", null=True, blank=True)
    keywords = models.CharField(_("Keywords"), max_length=2048, null=True, blank=True)
    body = models.TextField(_("Content"))

    class Meta:
        abstract = True

    def __str__(self):
        return self.title

    def get_tag_list(self):
        """
        A basic regular expression for converting keywords field to a list.
        """
        if self.keywords:
            return split(', ?', self.keywords)

    def save(self, *args, **kwargs):
        # Checks if there is a slug, if not calls slug_generator.
        if not self.slug:
            self.slug = slug_generator(self.__class__, self.title)
        super().save(*args, **kwargs)
